<?php

namespace Drupal\http_client_logger\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\http_client_logger\Event\HttpMetricsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Log HTTP performance metrics to watchdog.
 */
class LogMetrics implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Conversion factor for nano-seconds to seconds.
   */
  const NANOSECONDS_IN_SECONDS = 1000000000;

  /**
   * Contains the different date interval units.
   *
   * This array is keyed by strings representing the unit (e.g.
   * '1 year|@count years') and with the amount of values of the unit in
   * seconds.
   *
   * @var array
   */
  protected $units = [
    '1 hour|@count hours' => 3600,
    '1 min|@count min' => 60,
    '1 sec|@count sec' => 1,
    '1 ms|@count ms' => 0.001,
    '1 µs|@count µs' => 0.000001,
    '1 ns|@count ns' => 0.000000001,
  ];

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel for the module.
   */
  public function __construct(protected LoggerChannelInterface $logger) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[HttpMetricsEvent::class][] = ['logMetrics'];
    return $events;
  }

  /**
   * Log HTTP request performance metrics to watchdog.
   *
   * @param \Drupal\http_client_logger\Event\HttpMetricsEvent $event
   *   An event which reports HTTP metrics.
   */
  public function logMetrics(HttpMetricsEvent $event) {
    $message = [];
    $message[] = 'Search metrics:';
    $message[] = '<dl>';
    $message[] = '<dt>URL</dt> <dd>@url</dd>';
    $message[] = '<dt>Request method</dt> <dd>@method</dd>';
    $message[] = '<dt>Request size</dt> <dd>@request_size</dd>';
    $message[] = '<dt>Response size</dt> <dd>@response_size</dd>';
    $message[] = '<dt>Response code</dt> <dd>@response_code</dd>';
    $message[] = '<dt>Response time</dt> <dd>@response_time</dd>';
    $message[] = '</dl>';
    $message = implode("\n", $message);

    $this->logger->info($message, [
      '@url'           => $event->getUrl(),
      '@method'        => $event->getMethod(),
      '@request_size'  => (string) ByteSizeMarkup::create($event->getRequestSize()),
      '@response_size' => (string) ByteSizeMarkup::create($event->getResponseSize()),
      '@response_code' => $event->getResponseStatusCode(),
      '@response_time' => $this->formatInterval($event->getResponseTime()),
    ]);
  }

  /**
   * Extension of DateTimeFormatter::formatInterval with <1s granularity.
   */
  protected function formatInterval($interval, $granularity = 2, $langcode = NULL) {
    // Convert seconds to nano-seconds.
    $units = array_map(fn ($val) => $val *= self::NANOSECONDS_IN_SECONDS, $this->units);

    $output = '';
    foreach ($units as $key => $value) {
      $key = explode('|', $key);
      if ($interval >= $value) {
        $output .= ($output ? ' ' : '') . $this->formatPlural(floor($interval / $value), $key[0], $key[1], [], [
          'langcode' => $langcode,
        ]);
        $interval %= $value;
        $granularity--;
      }
      elseif ($output) {
        // Break if there was previous output but not any output at this level,
        // to avoid skipping levels and getting output like "1 year 1 second".
        break;
      }
      if ($granularity == 0) {
        break;
      }
    }
    return $output ? $output : $this->t('0 sec', [], [
      'langcode' => $langcode,
    ]);
  }

}
