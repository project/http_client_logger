<?php

namespace Drupal\http_client_logger\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\http_client_logger\Event\HttpTrafficEvent;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Log HTTP requests and responses to watchdog.
 */
class LogTraffic implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel for the module.
   */
  public function __construct(protected LoggerChannelInterface $logger) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[HttpTrafficEvent::class][] = ['logTraffic'];
    return $events;
  }

  /**
   * Log HTTP request performance metrics to watchdog.
   *
   * @param \Drupal\http_client_logger\Event\HttpTrafficEvent $event
   *   An event which reports HTTP metrics.
   */
  public function logTraffic(HttpTrafficEvent $event) {
    if ($event->getRequest()) {
      $this->logRequest($event->getRequest());

      if ($event->getResponse()) {
        $this->logResponse($event->getRequest(), $event->getResponse());
      }
    }
  }

  /**
   * Log an HTTP request.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   */
  protected function logRequest(RequestInterface $request) : void {
    if ($request->getBody()->isSeekable()) {
      $request->getBody()->rewind();
    }
    $requestBody = $request->getBody()->getContents();
    $requestBodyFormatted = $this->formatBody(
      $requestBody,
      $this->getContentType($request),
    );

    $message = [];
    $message[] = 'HTTP Request:';
    $message[] = '<dl>';
    $message[] = '<dt>URI</dt> <dd>@uri</dd>';
    $message[] = '<dt>Request method</dt> <dd>@method</dd>';
    $message[] = '<dt>Headers</dt> <dd><pre>@headers</pre></dd>';
    $message[] = '<dt>Body (raw)</dt> <dd><pre>@body</pre></dd>';
    $message[] = '<dt>Body (formatted)</dt> <dd><pre>@body_formatted</pre></dd>';
    $message[] = '</dl>';
    $message = implode("\n", $message);

    $this->logger->info($message, [
      '@uri'            => $request->getUri(),
      '@method'         => $request->getMethod(),
      '@headers'        => $this->getHeadersRaw($request->getHeaders()),
      '@body'           => $requestBody,
      '@body_formatted' => $requestBodyFormatted,
    ]);
  }

  /**
   * Log an HTTP response.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The HTTP response.
   */
  protected function logResponse(RequestInterface $request, ResponseInterface $response) : void {
    if ($response->getBody()->isSeekable()) {
      $response->getBody()->rewind();
    }
    $responseBody = $response->getBody()->getContents();
    $responseBodyFormatted = $this->formatBody(
      $responseBody,
      $this->getContentType($response),
    );

    $message = [];
    $message[] = 'HTTP Response:';
    $message[] = '<dl>';
    $message[] = '<dt>URI</dt> <dd>@uri</dd>';
    $message[] = '<dt>Response code</dt> <dd>@response_code</dd>';
    $message[] = '<dt>Headers</dt> <dd><pre>@headers</pre></dd>';
    $message[] = '<dt>Body (raw)</dt> <dd><pre>@body</pre></dd>';
    $message[] = '<dt>Body (formatted)</dt> <dd><pre>@body_formatted</pre></dd>';
    $message[] = '</dl>';
    $message = implode("\n", $message);

    $this->logger->info($message, [
      '@uri'            => $request->getUri(),
      '@method'         => $request->getMethod(),
      '@response_code'  => $response->getStatusCode(),
      '@headers'        => $this->getHeadersRaw($response->getHeaders()),
      '@body'           => $responseBody,
      '@body_formatted' => $responseBodyFormatted,
    ]);
  }

  /**
   * Get the raw headers as a string.
   *
   * @param array $headers
   *   The HTTP headers.
   *
   * @return string
   *   The headers, as a text block.
   */
  protected function getHeadersRaw(array $headers) : string {
    return implode("\n",
      array_map(
        fn ($key, $val) => $key . ': ' . implode(',', $val),
        array_keys($headers),
        array_values($headers),
    ));
  }

  /**
   * Format the body according to its content-type.
   */
  protected function formatBody($body, string|null $content_type) : ?string {
    if (!$content_type) {
      return NULL;
    }
    if (str_starts_with($content_type, 'application/x-www-form-urlencoded')) {
      return str_replace('&', "\n", urldecode($body));
    }
    if (str_starts_with($content_type, 'application/json')) {
      $decode = json_decode($body);
      return json_encode($decode, JSON_PRETTY_PRINT);
    }
    return NULL;
  }

  /**
   * Get the content-type header from a request or response.
   *
   * @param \Psr\Http\Message\MessageInterface $message
   *   A HTTP request or response.
   *
   * @return string|null
   *   The content-type header, if it exists.
   */
  protected function getContentType(MessageInterface $message) : ?string {
    if ($message->hasHeader('Content-Type')) {
      $value = $message->getHeader('Content-Type');
      if (is_string($value)) {
        return $value;
      }
      if (is_array($value)) {
        return implode(',', $value);
      }
    }
    return NULL;
  }

}
