<?php

namespace Drupal\http_client_logger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the HTTP client-logger service.
 */
class AdminConfigurationForm extends ConfigFormBase {

  /**
   * Config key which will store the configuration for the service.
   *
   * @var string
   */
  const CONFIG_KEY = 'http_client_logger.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => !($config->get('enabled') === 0),
    ];

    $form['destination'] = [
      '#type' => 'radios',
      '#title' => $this->t('Logging destination'),
      '#options' => [
        'common' => $this->t('Record HTTP requests, responses, and errors in the same file.'),
        'separate' => $this->t('Record HTTP requests, responses, and errors in separate files.'),
      ],
      '#default_value' => ($config->get('destination')) ?? 'common',
    ];

    $filenames = $config->get('filenames') ?? [
      'common' => '',
      'request' => '',
      'response' => '',
      'error' => '',
    ];

    $commonLogFileDefinition = [
      'enabled' => [
        ':input[name="destination"]' => ['value' => 'common'],
      ],
      'required' => [
        ':input[name="destination"]' => ['value' => 'common'],
      ],
    ];
    $separateLogFileDefinition = [
      'enabled' => [
        ':input[name="destination"]' => ['value' => 'separate'],
      ],
    ];

    $form['naming'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logging destination filename'),
    ];
    $form['naming']['filenames'] = [
      '#tree' => TRUE,
    ];
    $form['naming']['filenames']['common'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Common log file'),
      '#default_value' => $filenames['common'],
      '#states' => $commonLogFileDefinition,
    ];
    $form['naming']['filenames']['request'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Log file for requests'),
      '#default_value' => $filenames['request'],
      '#states' => $separateLogFileDefinition,
    ];
    $form['naming']['filenames']['response'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Log file for responses'),
      '#default_value' => $filenames['response'],
      '#states' => $separateLogFileDefinition,
    ];
    $form['naming']['filenames']['error'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Log file for errors'),
      '#default_value' => $filenames['error'],
      '#states' => $separateLogFileDefinition,
    ];

    // @todo Implement tokens in the log destination filename:
    // - datetime (inc yyyy/mm/dd etc)
    // - request_domain
    // - response_code
    // - response_status.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);

    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'http_client_logger.configuration_form';
  }

}
