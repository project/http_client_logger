<?php

namespace Drupal\http_client_logger\Event;

/**
 * Report the performance metrics for a HTTP request/response.
 */
class HttpMetricsEvent {

  /**
   * The request URI.
   *
   * @var string
   */
  protected string $uri;

  /**
   * The request method (e.g. HEAD, GET, DELETE).
   *
   * @var string
   */
  protected string $method;

  /**
   * The request size in bytes.
   *
   * @var int
   */
  protected int $requestSize;

  /**
   * The response size in bytes.
   *
   * @var int
   */
  protected int $responseSize;

  /**
   * The response time in nano-seconds.
   *
   * @var int
   */
  protected int $responseTime;

  /**
   * The response status code (e.g. 200, 404).
   *
   * @var int
   */
  protected int $responseStatusCode;

  /**
   * Set the URI.
   *
   * @param string $uri
   *   The request URI.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setUri(string $uri) : static {
    $this->uri = $uri;
    return $this;
  }

  /**
   * Set the request method.
   *
   * @param string $method
   *   The request method.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setMethod(string $method) : static {
    $this->method = $method;
    return $this;
  }

  /**
   * Set the request size.
   *
   * @param int $requestSize
   *   The request size in bytes.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setRequestSize(int $requestSize) : static {
    $this->requestSize = $requestSize;
    return $this;
  }

  /**
   * Set the URL.
   *
   * @param int $responseSize
   *   The response size in bytes.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setResponseSize(int $responseSize) : static {
    $this->responseSize = $responseSize;
    return $this;
  }

  /**
   * Set the response time in nano-seconds.
   *
   * @param int $responseTime
   *   The response time.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setResponseTime(int $responseTime) : static {
    $this->responseTime = $responseTime;
    return $this;
  }

  /**
   * Set the response status code.
   *
   * @param int $responseStatusCode
   *   The response status code.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setResponseStatusCode(int $responseStatusCode) : static {
    $this->responseStatusCode = $responseStatusCode;
    return $this;
  }

  /**
   * Get the URI.
   *
   * @return string
   *   The request URI.
   */
  public function getUrl() : string {
    return $this->uri;
  }

  /**
   * Get the request method.
   *
   * @return string
   *   The request method.
   */
  public function getMethod() : string {
    return $this->method;
  }

  /**
   * Get the request size.
   *
   * @return int
   *   The request size in bytes.
   */
  public function getRequestSize() : int {
    return $this->requestSize;
  }

  /**
   * Get the URL.
   *
   * @return int
   *   The response size in bytes.
   */
  public function getResponseSize() : int {
    return $this->responseSize;
  }

  /**
   * Get the response time in nano-seconds.
   *
   * @return int
   *   The response time.
   */
  public function getResponseTime() : int {
    return $this->responseTime;
  }

  /**
   * Get the response status code.
   *
   * @return int
   *   The response status code.
   */
  public function getResponseStatusCode() : int {
    return $this->responseStatusCode;
  }

}
