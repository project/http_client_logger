<?php

namespace Drupal\http_client_logger\Event;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Report HTTP request and response data.
 */
class HttpTrafficEvent {

  /**
   * The HTTP request.
   *
   * @var \Psr\Http\Message\RequestInterface
   */
  protected RequestInterface $request;

  /**
   * The HTTP response.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected ResponseInterface $response;

  /**
   * Set the request.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setRequest(RequestInterface $request) : static {
    $this->request = $request;
    return $this;
  }

  /**
   * Set the response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The HTTP response.
   *
   * @return static
   *   Return $this for method chaining.
   */
  public function setResponse(ResponseInterface $response) : static {
    $this->response = $response;
    return $this;
  }

  /**
   * Get the request.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   The HTTP request.
   */
  public function getRequest() : RequestInterface {
    return $this->request;
  }

  /**
   * Get the response.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The HTTP response.
   */
  public function getResponse() : ResponseInterface {
    return $this->response;
  }

}
