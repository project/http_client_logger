<?php

namespace Drupal\http_client_logger\GuzzleHttp;

use GuzzleHttp\MessageFormatter as GuzzleMessageFormatter;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Extend the standard Guzzle message-formatter with new formats.
 */
class MessageFormatter extends GuzzleMessageFormatter {

  /**
   * Default message formatter template for requests.
   *
   * @var string
   */
  const REQUEST_DEFAULT = "{req_body}\n";

  /**
   * Default message formatter template for responses.
   *
   * @var string
   */
  const RESPONSE_DEFAULT = "{res_body}\n";

  /**
   * Default message formatter template for errors.
   *
   * @var string
   */
  const ERROR_DEFAULT = "{error}\n";

  /**
   * Format a request (and optional response) using HAR format.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   * @param \Psr\Http\Message\ResponseInterface $response
   *   (optional) The HTTP response.
   * @param \Exception $error
   *   (optional) Any errors returned from the request.
   */
  protected function formatHar(RequestInterface $request, ResponseInterface $response = NULL, \Exception $error = NULL) {

  }

}
