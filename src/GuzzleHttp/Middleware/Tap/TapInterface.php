<?php

namespace Drupal\http_client_logger\GuzzleHttp\Middleware\Tap;

use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Interface used to tap HTTP requests via middleware.
 */
interface TapInterface {

  /**
   * Middleware to log requests and responses.
   *
   * @see \Drupal\Core\Http\HandlerStackConfigurator
   * @see \GuzzleHttp\Middleware:log()
   */
  public function __invoke() : callable;

  /**
   * The request is about to start.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   * @param array $options
   *   Options passed to the Guzzle HTTP client.
   *
   * @see \GuzzleHttp\Middleware::tap()
   */
  public function before(RequestInterface $request, array $options);

  /**
   * The response has been received.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   * @param array $options
   *   Options passed to the Guzzle HTTP client.
   * @param \GuzzleHttp\Promise\PromiseInterface $promise
   *   A promise which fulfils with a \GuzzleHttp\Psr7\Response.
   *
   * @see \GuzzleHttp\Middleware::tap()
   */
  public function after(RequestInterface $request, array $options, PromiseInterface $promise);

}
