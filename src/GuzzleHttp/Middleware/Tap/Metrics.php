<?php

namespace Drupal\http_client_logger\GuzzleHttp\Middleware\Tap;

use Drupal\http_client_logger\Event\HttpMetricsEvent;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Attach a middleware to record performance metrics.
 */
class Metrics {

  /**
   * The metrics event to report.
   *
   * @var \Drupal\http_client_logger\Event\HttpMetricsEvent
   */
  protected HttpMetricsEvent $event;

  /**
   * Constructor.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   */
  public function __construct(protected EventDispatcherInterface $eventDispatcher) {
    $this->event = new HttpMetricsEvent();
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke() : callable {
    return function (callable $handler): callable {
      return function (RequestInterface $request, array $options) use ($handler) {
        $start = hrtime(TRUE);

        /** @var \GuzzleHttp\Promise\PromiseInterface $promise */
        $promise = $handler($request, $options);
        $promise->then(function (ResponseInterface $response) use ($start, $request) {
          $duration = hrtime(TRUE) - $start;
          $this->report($duration, clone $request, clone $response);
        });
        return $promise;
      };
    };
  }

  /**
   * Report the metrics.
   *
   * @param int $duration
   *   The response time in nano-seconds.
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The HTTP response.
   */
  public function report(int $duration, RequestInterface $request, ResponseInterface $response) {
    $event = new HttpMetricsEvent();

    $requestSize = $request->getBody()->getSize() ?? 0;
    $requestSize += $this->getHeaderSize($request->getHeaders());

    $responseSize = $response->getBody()->getSize() ?? 0;
    $responseSize += $this->getHeaderSize($response->getHeaders());

    $event
      ->setUri($request->getUri())
      ->setMethod($request->getMethod())
      ->setRequestSize($requestSize)
      ->setResponseStatusCode($response->getStatusCode())
      ->setResponseSize($responseSize)
      ->setResponseTime($duration);

    $this
      ->eventDispatcher
      ->dispatch($event);
  }

  /**
   * Calculate the combined size of HTTP headers.
   *
   * @param array $headers
   *   The HTTP headers.
   *
   * @return int
   *   The size of the headers, in bytes.
   */
  protected function getHeaderSize(array $headers) {
    return array_reduce($headers, function ($size, $header) {
      $size += strlen(implode(': ', $header)) + 1;
    }, 0);
  }

}
