<?php

namespace Drupal\http_client_logger\GuzzleHttp\Middleware\Tap;

use Drupal\http_client_logger\Event\HttpTrafficEvent;
use GuzzleHttp\Middleware;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Attach a middleware to record the request and response data.
 */
class RequestResponse {

  /**
   * Constructor.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   */
  public function __construct(protected EventDispatcherInterface $eventDispatcher) {
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke() : callable {
    return Middleware::tap(
      NULL,
      [$this, 'after']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function after(RequestInterface $request, array $options, PromiseInterface $promise) : void {
    $promise
      ->then(function (ResponseInterface $response) use ($request) {
        $this->report(clone $request, clone $response);
      });
  }

  /**
   * Dispatch the event to subscribers.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The HTTP request.
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The HTTP response.
   */
  public function report(RequestInterface $request, ?ResponseInterface $response = NULL) : void {
    $event = new HttpTrafficEvent();
    $event
      ->setRequest($request)
      ->setResponse($response);

    $this
      ->eventDispatcher
      ->dispatch($event);
  }

}
